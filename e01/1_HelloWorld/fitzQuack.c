#include <iostream>
using namespace std;

int main()
{
    for (int i = 1; i <= 100; i++)
    {
        if (i%5 == 0)
        {
            cout << "Fitz ";
            continue;
        }
        else if ( i%7 == 0)
        {
            cout << "Quack ";
            continue;
        }

        cout << i << " ";
    }

    return 0;
}
