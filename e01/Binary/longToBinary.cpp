#include <iostream>

using namespace std;

int main() {

    const unsigned long BITS_PER_BYTE = 8UL;
    unsigned long maske = 0x1UL << (BITS_PER_BYTE * sizeof(long) - 1UL);
    unsigned long hilf;
    long zahl;

    cout << "Bitte long-Zahl eingeben:" << endl;
    cin >> zahl;
    hilf = zahl;

    cout << "Zahl " << zahl << " in Binärdarstellung: " << endl;
    for (unsigned int i = 1; i <= BITS_PER_BYTE * sizeof(long); i++) {
        if ((hilf & maske) == 0)
            cout << "0";
        else
            cout << "1";
        maske >>= 1;
    }
    cout << "." << endl;

    return 0;
}
