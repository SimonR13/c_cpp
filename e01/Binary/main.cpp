#include <iostream>

using namespace std;

int main()
{
    const unsigned int BITS_PER_BYTE = 8UL;
    long zahl, einsen, i;

    do {
        cout << "Bitte long-Zahl eingeben: " << endl;
        cin >> zahl;
        cout << endl;

        einsen = 0;
        if (zahl >= 7) {
            i = BITS_PER_BYTE * sizeof(long);
            while (i-- > 0 && einsen < 3) {
                einsen = ((zahl & 0x1L) != 0) ? einsen + 1 : 0;
                zahl >>= 1;
            }

            if (einsen >= 3) {
                cout << "Zahl hat 1er-Drilling." << endl;
            }
            else {
                cout << "Zahl hat keine 1er-Drillinge." << endl;
            }
        }
        else {
            cout << "Zahlen unter 7 können keine 1er-Drillinge besitzen..." << endl;
        }

    } while (zahl >= 0);

    return 0;
}
