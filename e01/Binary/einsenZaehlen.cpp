#include <iostream>
#include <limits.h>

using namespace std;

int main() {
    long zahl, einsen, i;
    const unsigned int BITS_PER_BYTE = 8UL;

    do {
        cout << "Bitte long-Zahl eingeben: " << endl;
        cin >> zahl;
        cout << endl;

        einsen = 0;
        i = BITS_PER_BYTE * sizeof(long);
        while (i-- > 0 && zahl > 0) {
            einsen = ((zahl & 0x1L) != 0) ? einsen + 1 : einsen;
            zahl >>= 1;
        }

        cout << "Zahl hat " << einsen << " 1er." << endl;

    } while (zahl >= 0);

    return 0;
}
