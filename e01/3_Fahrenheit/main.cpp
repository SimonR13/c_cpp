#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    cout << left << setw(12) << setfill(' ') << "Fahrenheit";
    cout << "|";
    cout << right << setw(12) << setfill(' ') << "Celsius" << endl;
    cout << setw(25) << setfill('-') << endl;

    for (int f = 0; f <= 300; f+=20) {
        double c = (5.0/9.0) * (f - 32);

        cout << left << setw(12) << setfill(' ') << f;
        cout << "|";
        cout << right << setw(12) << setfill(' ') << c << endl;

    }

    return 0;
}
