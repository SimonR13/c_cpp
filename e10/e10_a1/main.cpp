#include <iostream>

using namespace std;

#include "kreis.h"
#include "rechteck.h"

int main()
{
    cout << "Klassen Rechteck und Kreis testen:" << endl;
    Rechteck box(4, 5);
    box.setKanten(box.getLaenge() * 2, box.getBreite() * 2);
    cout << "Flaeche: " << box.flaeche()
         << " Umfang: " << box.umfang() << endl;

    double len, wid, randFlaeche, randUmfang;
    cin >> len;
    cin >> wid;
    Rechteck smBox(len, wid);
    Rechteck lgBox(len + 1.0, wid + 1.0);

    randFlaeche = lgBox.flaeche() - smBox.flaeche();
    randUmfang = lgBox.umfang() + smBox.umfang();
    cout << "Randflaeche: " << randFlaeche
         << " Randumfang: " << randUmfang << endl;


    Kreis innen(2);
    Rechteck aussen(4,4);

    cout << "Flaeche: " << (aussen.flaeche() - innen.flaeche()) << endl;
    cout << "Umfang: " << (aussen.umfang()) << endl;

    return 0;
}
