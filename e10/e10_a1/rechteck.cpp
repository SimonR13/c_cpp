#include "rechteck.h"
#include <cmath>

Rechteck::Rechteck(double l, double b)
{
    laenge = l;
    breite = b;
}

double Rechteck::getLaenge()
{
    return laenge;
}

double Rechteck::getBreite()
{
    return breite;
}

void Rechteck::setLaenge(double l)
{
    laenge = l;
}

void Rechteck::setBreite(double b)
{
    breite = b;
}

void Rechteck::setKanten(double l, double b)
{
    laenge = l;
    breite = b;
}

double Rechteck::flaeche()
{
    return(laenge * breite);
}

double Rechteck::umfang()
{
    return(2.0 * (laenge+breite));
}
