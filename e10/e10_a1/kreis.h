#ifndef KREIS_H_INCLUDED
#define KREIS_H_INCLUDED

class Kreis
{
    private:
        double radius;
    public:
        Kreis(double r);
        double getRadius();
        void setRadius(double r);
        double flaeche();
        double umfang();
};

#endif // KREIS_H_INCLUDED
