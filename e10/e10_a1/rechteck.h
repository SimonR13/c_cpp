#ifndef RECHTECK_H_INCLUDED
#define RECHTECK_H_INCLUDED

class Rechteck
{
    private:
        double laenge, breite;
    public:
        Rechteck(double l, double b);
        double getLaenge();
        double getBreite();
        void setLaenge(double l);
        void setBreite(double b);
        void setKanten(double l, double b);
        double flaeche();
        double umfang();
};

#endif // RECHTECK_H_INCLUDED

