#include "kreis.h"
#include <cmath>

Kreis::Kreis(double r):radius(r)
{
    // nothing to do
}

double Kreis::getRadius()
{
    return radius;
}

void Kreis::setRadius(double r)
{
    radius = r;
}

double Kreis::flaeche()
{
    return(M_PI * radius * radius);
}

double Kreis::umfang()
{
    return(2.0 * M_PI * radius);
}
