#include <iostream>
#include <string>
using namespace std;

struct player_strct {
    int playerId;
    string name;
    long highscore; }
    allespieler[8];

int main(void) {
    player_strct myPlayer = {2, "InitMeier", 0};
    allespieler[2].name = "Mueller";
    player_strct *mPlPtr;
    mPlPtr = &myPlayer;
    mPlPtr->highscore = 10;

    cout << "mPlPtr: " << (*mPlPtr).name << endl;
    cout << "allespieler[2]: " << allespieler[2].name << endl;
    
    allespieler[1] = myPlayer;
    cout << "allespieler[1]: " << allespieler[1].name << endl;
    allespieler[0] = *mPlPtr;
    cout << "allespieler[0]: " << allespieler[0].name << endl << endl;

    allespieler[0].name = "allespielerMeier";
    cout << "allespieler[0]: " << allespieler[0].name << endl;
    cout << "mPlPtr: " << (*mPlPtr).name << endl;
    cout << "myPlayer: " << myPlayer.name << endl << endl;

    myPlayer.name = "myPlayerMeier";
    cout << "allespieler[0]: " << allespieler[0].name << endl;
    cout << "mPlPtr: " << (*mPlPtr).name << endl;
    cout << "myPlayer: " << myPlayer.name << endl << endl;
    
    (*mPlPtr).name = "PtrMeier";
    cout << "allespieler[0]: " << allespieler[0].name << endl;
    cout << "mPlPtr: " << (*mPlPtr).name << endl;
    cout << "myPlayer: " << myPlayer.name << endl;

    return 0;
}
