#include <iostream>
#include <iomanip>
using namespace std;

#include "ascii_zeichen.h"

void print_Ascii_Zeichen() {
    int nameWidth = 3;
    char separator = ' ';
    cout << left << setw(nameWidth) << setfill(separator) << " ";
    for (int i = 0; i<8; i++) {
        cout << left << setw(nameWidth) << setfill(separator) << i;
    }
    cout << endl;

    int hexChar = 0x0;
    for (int i = 0; i<0xf; i++) {
        cout << left << setw(nameWidth) << setfill(separator) << i;
        for (int j = 0; j<0x8; j++) {
            if ((16*j) + hexChar < 0x20) {
                cout << right << setw(2) << setfill(separator) << ".";
            }
            cout << right << setw(3) << setfill(separator) << (char) ((16*j)+hexChar);
        }
        hexChar++;
        cout << endl;
    }

}
