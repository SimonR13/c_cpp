#include <iostream>
#include <cstdlib>
//#include <cstddef>
#include <ctime>
#include "pi.h"

using namespace std;

bool is_Treffer(double _x, double _y) {

    double pos = double(_x * _x) + double(_y * _y);

    return (pos <= 1.0);
}

double get_Naeherungswert(double _treffer, double _anzahl) {
    double quarter = _treffer / double(_anzahl);

    return (4 * quarter);
}

double calculate_PI() {
    srand(time(NULL) * time(NULL));

    int anzahl=0, treffer=0;
    cout << "Anzahl der Zufallswerte :";
    cin >> anzahl;

    for (int i = 0; i < anzahl; i++) {

        double x = double(rand()) / double(RAND_MAX);
        double y = double(rand()) / double(RAND_MAX);

        if (is_Treffer(x, y))
            treffer++;
    }

    cout << "Es wurden " << anzahl << " Punkte generiert." << endl;
    cout << "Davon befinden sich " << treffer << " im Einheitskreis." << endl;
    cout << "Daraus folgt, dass PI ~= " << get_Naeherungswert(double(anzahl), double(treffer)) << endl;
}
