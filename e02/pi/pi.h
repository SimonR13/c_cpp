#ifndef PI_H_INCLUDED
#define PI_H_INCLUDED

bool is_Treffer(double x, double y);

double get_Naeherungswert(double treffer, double anzahl);

double calculate_PI();

#endif // PI_H_INCLUDED
