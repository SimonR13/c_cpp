#include <iostream>
#include "mittelwert.h"

using namespace std;

int get_Mittelwert() {
    unsigned int intIn, i=0, summe=0;

    cout << "Zahlen fuer mittelwert eingeben: " << endl;
    cin >> intIn;
    while (intIn != 0) {
        summe += intIn;
        i++;
        cin >> intIn;
    }

    return (summe/(i<1 ? 1 : i));
}
