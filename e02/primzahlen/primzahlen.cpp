#include <iostream>
#include <cmath>

using namespace std;

#include "primzahlen.h"

bool *init_primArray(int length) {
    bool *pTmp;
    pTmp = (bool *) new bool[length*sizeof(bool)];

    for (int i=0; i<length; i++) {
        pTmp[i] = true;
    }

    return(pTmp);
}

void get_Primzahlen() {
    int anzahlPrims = 0;

    cout << "Wertebereiche 0...? ";
    cin >> anzahlPrims;
    cout << endl;

    bool *pPrimArr = init_primArray(anzahlPrims);

    // primzahlen bestimmen
    int prim = 2;
    do {
        for (int i=2; i<=anzahlPrims; i++) {
            if (prim*i > anzahlPrims) break;
            pPrimArr[prim*i] = false;
        }
        prim++;
    } while (prim<=anzahlPrims);

    for (int i=1; i<anzahlPrims; i++) {
        cout.width(3);
        cout << i << ":" << pPrimArr[i] << endl;
    }

}
