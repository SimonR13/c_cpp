#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc <= 1) return(0);

    string filePath = argv[1];
    ifstream quelle;
    cout << "Open :" << filePath.c_str() << endl;
    quelle.open(filePath.c_str(), ios::binary|ios::in);

    if (!quelle) {
        cerr << filePath.c_str() << endl << " kann nicht geoeffnet werden..." << endl;
        exit(-1);
    }

    int countChar=0, countWords=0, countLines=0;
    bool word=false;

    char tmpChar;
    while (quelle.get(tmpChar)) {
        int asciiCode = (int) tmpChar;
        if (asciiCode == 0x0A) {
            countLines++;
            countWords++;
            word = false;
            continue;
        }

        countChar++;

        if ((asciiCode > 0x20 & asciiCode <= 0x5A) | (asciiCode >= 0x61 & asciiCode < 0x7A)) {
            word = true;
        }

        if (word & (asciiCode <= 0x20 || asciiCode >= 0x5A) & (asciiCode <= 0x61 || asciiCode >=0x7A)) {
            word = false;
            countWords++;
        }
    }

    quelle.close();

    cout << "Anzahl Zeichen: " << countChar << endl;
    cout << "Anzahl Woerter: " << countWords << endl;
    cout << "Anzahl Linien: " << countLines << endl;

    return (0);
}
