#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc <= 1) return(0);

    string filePath = argv[1];
    string outputFilePath = string("noWhitespace_") + filePath.c_str();

    ifstream quelle;
    ofstream senke;
    cout << "Open :" << filePath.c_str() << endl;
    quelle.open(filePath.c_str(), ios::binary|ios::in);
    cout << "Open: " << outputFilePath.c_str() << endl;
    senke.open(outputFilePath.c_str(), ios::binary|ios::out);

    if (!quelle) {
        cerr << filePath.c_str() << endl << " kann nicht geoeffnet werden..." << endl;
        exit(-1);
    }
    if (!senke) {
        cerr << outputFilePath.c_str() << endl << " kann nicht geoffnet werden.." << endl;
        exit(-2);
    }

    string line;
    while (getline(quelle,line,'\n')) {
        char firstChar = line[0];
        if (firstChar == (char) 0x0A | firstChar == '\0') continue;

        while (firstChar == 0x09 | firstChar == 0x20) {
            line = line.substr(1,line.size());
            firstChar = line[0];
            cout << firstChar << endl;
        }
        senke << line << endl;
    }

    quelle.close();
    senke.close();

    return(0);
}
