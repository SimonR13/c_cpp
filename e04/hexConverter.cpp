#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    string filePath = argv[1];
    ifstream quelle;

    quelle.open(filePath.c_str(), ios::binary|ios::in);

    char buffer[1];
    while (quelle >> buffer) {
        string outStr = buffer;
        cout << outStr.c_str();
    }

    quelle.close();

    return(0);
}
