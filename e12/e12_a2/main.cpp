#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;


// eine Klasse zur Verwaltung von Automobilen
class Auto {		// eine Klasse zur Verwaltung von Automobilen
  public:
    // Deklaration von Konstruktoren, Destruktoren etc.
    // ...
    Auto();
    Auto(const int leistung, const char* hersteller);
    Auto(const Auto &a);
    ~Auto();
    Auto& operator= (const Auto &a);
    friend ostream& operator<<(ostream &ostr, const Auto &a);

    static int anzahl;		// eine Klassenvariable:
						    // die Anzahl der vorhandenen Autos

  private:
    int leistung;			// Leistung in kw
    char *hersteller;			// Hersteller, z.B. VW, Opel etc.
};

// b) den Standardkonstruktor
Auto::Auto()
{
//
    anzahl++;
    // start verbesserung lt musterloesung
    leistung = 0;
    hersteller = new char[10];
    strcpy(hersteller, "unbekannt");
    // verbesserung ende
}

// c) den allgemeinen Konstruktor
Auto::Auto(const int leistung, const char *hersteller)
{
//
    anzahl++;

//    Auto::leistung = leistung;
//    if (hersteller == NULL) Auto::hersteller = "";
//    else Auto::hersteller = const_cast<char*>(hersteller);

    // muesterloesung
      this->leistung = leistung;
      if(hersteller == NULL)
        hersteller = "";
      this->hersteller = new char[strlen(hersteller) + 1];
      strcpy(this->hersteller, hersteller);
}

// d) den Kopierkonstruktor
Auto::Auto(const Auto &a)
{
//
    anzahl++;
    leistung = a.leistung;
    hersteller = new char[strlen(a.hersteller)+1];
    strcpy(hersteller, a.hersteller);
}

// e) den Destruktor
Auto::~Auto()
{
    delete [] hersteller; // vorher nicht moeglich weil standardkonstruktor 'falsch'
    anzahl--;
}

// f) den Zuweisungsoperator =
Auto& Auto::operator=(const Auto &a)
{
  // Zuweisung identischer Objekte vermeiden
    if (this == &a) return(*this);

    delete[] hersteller; // s. destruktor
    hersteller = new char[strlen(a.hersteller)+1];
    strcpy(hersteller, a.hersteller);
    leistung = a.leistung;
    return(*this);
}

// g) den Ausgabeoperator <<
ostream& operator<<(ostream &ostr, const Auto &a)
{
  return ostr << "Hersteller: " << (strcmp(a.hersteller,"")?a.hersteller:"UFO") << ";  "
              << "Leistung: "   << a.leistung   << " kw;"
              << endl;
}

// a) Klassenvariable anzahl definieren und initialisieren
int Auto::anzahl = 0;
/// ??

int main()
{
  cout << "Anzahl Autos: " << Auto::anzahl << endl;
  Auto a;
  Auto b(85, "Lancia");
  cout << "Anzahl Autos: " << Auto::anzahl << endl;
  Auto c = b;
  Auto d = Auto(110, "Alfa Romeo");
  cout << "Anzahl Autos: " << Auto::anzahl << endl;
  a = b = c;
  d = d;
  cout << c << d;
  Auto e(170, NULL);
  Auto f(e);
  cout << e << f;
  cout << "Anzahl Autos: " << Auto::anzahl << endl;
}

/*
Ausgabe des Programms:

Anzahl Autos: 0
Anzahl Autos: 2
Anzahl Autos: 4
Hersteller: Lancia;  Leistung: 85 kw;
Hersteller: Alfa Romeo;  Leistung: 110 kw;
Hersteller: UFO;  Leistung: 170 kw;
Hersteller: UFO;  Leistung: 170 kw;
Anzahl Autos: 6

*/
