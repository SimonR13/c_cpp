#include <iostream>
#include <string>
using namespace std;

class Basis
{
    private:
        string msg;
    protected:
        int n;
    public:
        Basis(string s, int m = 0) : msg(s), n(m) {}

        void output()
        {
            cout << n << " " << msg << endl;
        }
};

class Abgeleitet : public Basis
{
    private:
        int n;
    public:
        Abgeleitet(int m = 1) : Basis("Basis", m-1), n(m) {}

        void output()
        {
            cout << n << endl;
            Basis::output();
        }
};

int main()
{
    Basis bObj("Basisklasse", 1);
    Abgeleitet aObj1(4), aObj2;
    bObj.output();                  // 1 Basisklasse
    aObj1.output();                 // 4
                                    // 3 Basis
    aObj2.output();                 // 1
                                    // 0 Basis
    return 0;
}

