#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

class String
{
    private:
        char* ptr;
        unsigned len;
    public:
        String() {
            ptr = new char[1];
            len = 0;
            *ptr = 0x0;
        }
        String(const char* s) {
            if (s == NULL) s = "";
            len = strlen(s);
            ptr = new char[len+1];
            strcpy(ptr, s);
        }
        String(const String& s) {
            len = s.len;
            ptr = new char[len+1];
            strcpy(ptr, s.ptr);
        }
        ~String() {
            delete[] ptr;
        }

        String& operator= (const String& s) {
            if (this == &s) return(*this);
            delete[] ptr;
            len = s.len;
            ptr = new char[len+1];
            strcpy(ptr, s.ptr);
            return(*this);
        }
        String& operator= (const char* s) {
            if (ptr == s) return (*this);
            if (s == NULL) s = "";
            delete[] ptr;
            len = strlen(s);
            ptr = new char[len+1];
            strcpy(ptr, s);
            return(*this);
        }

        char* getPtr() const { return ptr; }

//        istream& operator>> (istream& istr, String& s);
//        ostream& operator<< (ostream& ostr, const String& s);

        String& operator +=(const String& s);
        String& operator *=(int i);
};

istream& operator>> (istream& istr, String& s)
{
    char buffer[500];
    istr >> buffer;
    s = buffer;
    return istr;
}

ostream& operator<< (ostream& ostr, const String& s)
{
    return(ostr << s.getPtr() << endl);
}

int main()
{
//    Uebung 11 Aufgabe 1
//    String a;
//    String b = a;
//    String c = "Kernighan";
//    String d = String("Ritchie");
//    String e = (char*) 0;
//
//    a = "Waalkes";
//    a = b = c;
//    d = d;
//    d = d.getPtr();
//
//    cout << c << d;

//  Uebung 12 Aufgabe 1
    String s = "Abc";
    String t = String("Qwe");
    s += s += t *= 3;
    cout << s << t;
    t *= 1;
    cout << "t == " << t << endl;
    t *= 0;
    cout << "t == " << t << endl;

    return 0;
}

String& String::operator +=(const String& s)
{
    if (s.len == 0) return(*this);
    char* pTmp;
    pTmp = new char[len+1];
    strcpy(pTmp, ptr);
    delete[] ptr;
    ptr = new char[len+s.len+1];
    strcpy(ptr, pTmp);
    strcpy((ptr+len), s.ptr);
    len = len + s.len;
    delete[] pTmp;
    return(*this);
}

String& String::operator *=(int i)
{
    if (i==1) return(*this);

    if (i==0)
    {
        delete[] ptr;
        len = 0;
        ptr = new char[1];
        *ptr = 0x0;
        return(*this);
    }

    char* pTmp = new char[len+1];
    strcpy(pTmp, ptr);
    delete[] ptr;
    ptr = new char[(len*i)+1];
    for (int c=0; c<i; c++)
    {
        strcpy((ptr+(c*len)), pTmp);
    }
    len = (len * i);
    delete[] pTmp;
    return(*this);
}
