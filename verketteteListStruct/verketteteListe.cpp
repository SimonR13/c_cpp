#include <iostream>
#include <string>
using namespace std;

struct list_strct {
    int i1; // zaehler
    string *pBegriff; // payload
    list_strct *next; // verkettung
};

int main(void) {
    list_strct *pStart = NULL;
    list_strct *pListe = NULL;
    string tmpString;
    int listCounter;
// FUELLEN der liste
    do {
        getline(cin,tmpString,'\n');
        if (pStart == NULL) {
            pStart = (list_strct *) new list_strct* [sizeof(list_strct)]; // malloc(sizeof(list_strct));
            pStart->pBegriff = new string(tmpString);
            pStart->i1 = listCounter++;
            pStart->next = NULL;
            pListe = pStart;
        }
        else {
            //pListe->next = (list_strct *) malloc(sizeof(list_strct));
            pListe->next = (list_strct *) new list_strct* [sizeof(list_strct)];
            pListe = pListe->next;
            pListe->pBegriff = new string(tmpString);
            pListe->i1 = listCounter++;
            pListe->next = NULL;
        }
    } while (tmpString != "ENDE");

// AUSGABE der liste
    pListe = pStart;
    while (pListe != NULL) {
        cout << pListe->i1 << " " << *(pListe->pBegriff) << endl;
        pListe = pListe->next;
    }

// FREIGABE
    pListe = pStart;
    while (pListe != NULL) {
        pStart = pListe;
        pListe = pListe->next;
        delete pStart->pBegriff;
        delete pStart;
    }
    pStart = NULL;

    return (0);
}
