#include <iostream>
using namespace std;
#include <stdlib.h>

const int MAX=5;

int main()
{
	int feld[MAX], hilf;
	int i, j, k;

	srand(56); //Zufallsgenerator vorbereiten
	for (i=0; i<MAX; i++)
	{
		feld[i]=rand() % 100+1;
		cout << feld[i] << " ";
	}
	cout << endl;

	for (i=MAX-1; i>0; i--)
	{
		for (j=0; j<i; j++)
		{
			cout << "(" << j << "-" << j+1 << "): ";
			if (feld[j]>feld[j+1])
			{
				hilf=feld[j];
				feld[j]=feld[j+1];
				feld[j+1]=hilf;
			}
			cout << feld[j] << " - " << feld[j+1] << " ";
		}
		cout << endl << MAX-1 << ". Druchlauf beendet: ";
		for (k=0; k<MAX; k++)
		{
			cout << feld[k] << " ";
		}
		cout << endl;
	}
}
