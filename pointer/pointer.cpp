#include<iostream>
using namespace std;

int main(void) {
    int i1 = 1;
    int i2 = 2;
    char text[] = "Hallo Test";

    int *pi1;
    char *pc1;

    pi1 = &i1;
    pc1 = &text[0];

    cout << i1 << " " << text << endl;

    *pc1 = '_';
    cout << text << endl;

    *(pc1+2) = '*';
    cout << text << endl;

    pc1[4] = '#';
    cout << text << endl;

    cout << pc1 << endl;
    cout << *(pc1 + (sizeof(text)/sizeof(char) -2)) << endl;
    cout << pc1 + (sizeof(text)/sizeof(char) -2) << endl;
    cout << pc1[sizeof(text)/sizeof(char) -1] << endl;
    cout << pc1[sizeof(text)/sizeof(char) -2] << endl;
    cout <<  (sizeof(text)/sizeof(char) -1) << endl;
    return 0;
}
