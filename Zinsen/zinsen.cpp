/*
 * zinsen.cpp
 *
 *  Created on: Oct 21, 2012
 *      Author: simon
 */


#include <iostream>
using namespace std;

int main() {
	short int zinssatz=0, einzahlung=5000;
	int konto=0, zuwachs=0, jahre=0;
	const short int startjahr=2012;

	cout << "Wie viele Jahre sollen berechnet werden?" << endl;
	cin >> jahre;
	if (jahre < 1) {jahre = 10;}
	cout << "Zu welchem Zinssatz?" << endl;
	cin >> zinssatz;
	if (zinssatz < 1) { zinssatz = 5;}
	cout << "Jahr     Zuwachs     Kontostand" << endl;
	cout << "---------------------------------------------" << endl;
	for (short int i=0; i <= jahre; i++) {
		zuwachs = konto * zinssatz / 100;
		konto = konto + zuwachs + einzahlung;
		cout << startjahr + i << "     " << zuwachs << "      " << konto << endl;
		zuwachs = 0;
	}
}
