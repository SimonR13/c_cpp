#include <iostream>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
    if (argc <= 1) return(0);

    for (int i=0; i<argc; i++) {
        char* arg;
        arg = argv[i];
        if (*(arg) != 0x21) continue;
        while (*(++arg) != 0x0) {
            //nothing to do
        }
        if (*(--arg) != 0x21) continue;

        cout << "Hat !:" << argv[i] << endl;
    }

    return(0);
}
