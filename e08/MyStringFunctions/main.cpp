#include <iostream>
#include <cstring>

using namespace std;

#include "my_functions.h"

//int main()
//{
//    cout << "Hello world!" << endl;
//    char* testString1;
//    char* testString2;
//    testString1 = "einmal eins";
//    testString2 = "zweimal zwei";
//
//    cout << "Laenge 1: " << my_strlen(testString1) << endl;
//    cout << "Laenge 2: " << my_strlen(testString2) << endl;
//    char* neuerString1;
//    neuerString1 = my_strcpy(neuerString1, testString1);
//    cout << "Neuer String: " << neuerString1 << endl;
//    neuerString1 = my_strcat(neuerString1, testString2);
//    cout << "2 an Neue 1: " << neuerString1 << endl;
//    char* pPos = my_strchr(neuerString1, char('n'));
//    cout << "Zeichen " << *pPos << " in Zeiger: " << pPos << endl;
//    int comp12 = my_strcmp(testString1, testString2);
//    int comp11 = my_strcmp(testString1, testString1);
//    cout << "1.compare(2): " << comp12 << endl;
//    cout << "1.compare(1): " << comp11 << endl;
//
//    return 0;
//}

int main()
{
   const char *str1 = "Wie man sich bettet, so liegt man.";
   const char *str2 = "Wie man in den Wald hineinruft, so schallt es heraus.";
   char ziel[256];
   char *tmp;

   cout << "Die Zeichenkette \""
        << str1
        << "\" enthaelt "
        << my_strlen(str1)
        << " Zeichen."
        << endl;

   my_strcpy(ziel, str1);
   cout << ziel << "\n" << endl;

   my_strcpy(ziel + 21, str2 + 32);
   cout << ziel << "\n" << endl;

   my_strcat(ziel, " ");
   my_strcat(ziel, str2);
   cout << ziel << "\n" << endl;

   tmp = my_strchr(ziel, ',');
   cout << (tmp == NULL ? "NULL" : tmp) << endl;
   tmp = my_strchr(ziel, '5');
   cout << (tmp == NULL ? "NULL" : tmp) << "\n" << endl;

   cout << my_strcmp(str1, str2) << endl;

   return 0;
}
