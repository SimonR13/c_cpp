#ifndef MY_FUNCTIONS_H_INCLUDED
#define MY_FUNCTIONS_H_INCLUDED

size_t my_strlen(const char *s);
//Liefert die Anzahl der Zeichen, die in s enthalten sind. Das abschließende Null-Byte wird
//nicht mitgezählt.
char *my_strcpy(char *dest, const char *src);
//Kopiert die Zeichenkette src Zeichen für Zeichen in die Zeichenkette dest. Das ab-
//schließende Null-Byte wird auch kopiert. Liefert dest als Funktionswert zurück.
char *my_strcat(char *dest, const char *src);
//Hängt eine Kopie der Zeichenkette src an die Zeichenkette dest an. Liefert dest als
//Funktionswert zurück.
char *my_strchr(const char *s, char c);
//Liefert einen Zeiger auf das erste Vorkommen des Zeichens c in der Zeichenkette s zurück
//oder aber NULL, falls c nicht in s enthalten ist.
int my_strcmp(const char *s1, const char *s2);
//Vergleicht die Zeichenketten s1 und s2; liefert einen Wert < 0, wenn s1<s2 ist, den Wert 0,
//wenn s1==s2 ist, und einen Wert > 0, wenn s1>s2 ist.

#endif // MY_FUNCTIONS_H_INCLUDED
