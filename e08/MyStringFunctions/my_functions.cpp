#include <cstdlib>
#include <cstring>

using namespace std;

#include "my_functions.h"

/*
* richtig
*/
size_t my_strlen(const char* s) {
    size_t retLen = 0;
    while (*(s+retLen) != 0x0) {
        ++retLen;
    }
    return retLen;
}

/*
* eigentlich falsch, da kein neuer speicherbereich benutzt werden sollte
* sondern in dest-speicher schreiben...
*/
char* my_strcpy(char* dest, const char* src)
{
//    size_t lenSrc = my_strlen(src);
//    if (lenSrc > 0 & dest != src) // return dest;
//    {
//        dest = (char*) malloc(lenSrc+1);
//        for (int i=0; i<lenSrc; i++)
//        {
//            dest[i] = *(src+i);
//        }
//        dest[lenSrc] = 0x0;
//    }

/*
*   Verbesserung
*/
    for (int i=0; (*(src+i) != 0x0); i++)
    {
        dest[i] = src[i];
    }

    return dest;
}

/*
* eigentlich falsch, da kein neuer speicherbereich benutzt werden sollte
* sondern in dest-speicher schreiben...
*/
char* my_strcat(char* dest, const char* src) {
//    size_t lenDest = my_strlen(dest);
//    size_t lenSrc = my_strlen(src);
//    if (lenSrc<1) return(dest);
//    if (lenDest<1) return (my_strcpy(dest,src));
//    char* tmp;
//    tmp = (char*) malloc(lenDest+lenSrc+1);
//    for (int i=0; i<lenDest; i++)
//    {
//        tmp[i] = *(dest + i);
//    }
//    for (int i=lenDest; i<(lenDest+lenSrc); i++)
//    {
//        tmp[i] = *(src+(i-lenDest));
//    }
//    tmp[lenDest+lenSrc] = 0x0;
//    dest = tmp;
/*
*   Verbesserung
*/
    int destEnd;
    for (destEnd=0; (*(dest+destEnd) != 0x0); destEnd++) ;

    int srcIter=0;
    while (src[srcIter] != 0x0)
    {
        dest[destEnd++] = src[srcIter++];
    }

    return(dest);
}

/*
*   Richtig, bis auf const_cast<char *>(s) + i;
*/
char* my_strchr(const char* s, char c) {
    int i=0;
    while(s[i++] != 0x0)
    {
        if (s[i] == c) return const_cast<char *>(s) + i;
    }
    return NULL;
}

/*
*   Richtig
*/
int my_strcmp(const char* s1, const char* s2) {
    int iter = 0;
    do
    {
        if (s1[iter] > s2[iter]) return(1);
        if (s1[iter] < s2[iter]) return(-1);
        if (s1[iter] == s2[iter]) ++iter;
    } while (s1[iter] != 0x0 & s2[iter] != 0x0);

    return(0);
}
