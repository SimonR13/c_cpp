#include <iostream>
using namespace std;

// Reihenfolge der Feldelemente von arr umkehren
void reverseArray(int *arr, int n);
// Feldelemente von arr ausgeben
void writeArray(int *arr, int n);

int main() {
    int intArray[6] = {2, 6, 9, 3, 5, 8};
    cout << "int-Array (vorher): " << endl;
    writeArray(intArray, 6);
    reverseArray(intArray, 6);
    cout << "int-Array (nachher): " << endl;
    writeArray(intArray, 6);
    cout << endl;
    return 0;
}

void reverseArray(int *arr, int n) {
    int *arrHinten;
    int temp;

    arrHinten = arr + (n-1);
    if ((n%2) == 0) n--;

    for (int i=0; i<(n/2); i++) {
        temp = *(arr+i);
        *(arr+i) = *(arrHinten-i);
        *(arrHinten-i) = temp;
    }
}

void writeArray(int *arr, int n) {
    // int *end;
    for (int i=0; i<n; i++) {
        cout << "Wert an Stelle " << i << ": " << *(arr + i) << endl;
    }
}
