#include <iostream>
#include <cmath>
#include <time.h>

using namespace std;

int main() {
    int lottozahlen[6] = {-1};

    srand((unsigned)time(NULL));

    for (int i=0; i<6; i++){
        int zahl = (rand() % 49) + 1;
        lottozahlen[i] = zahl;
    }

    for (int i=0; i<6; i++){
        cout << lottozahlen[i] << " ";
    }

    cout << endl;

    return(0);
}
