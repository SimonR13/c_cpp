#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;


struct Node {
  int info;
  Node *next;
};

// Die globale Variable head ist der Zeiger auf den Listenanfang.
Node *head = NULL;

void error(const string& s)
{
  cerr << s << endl;
}

void add_front(int info)
{
  Node *tmp;

  tmp = new Node;
  if (tmp == NULL)
    error("add_front: out of memory");
  tmp->info = info;
  tmp->next = head;
  head = tmp;
}

void delete_front()
{
    Node *tmp;

    if (head == NULL)
        error("delete_front: list is empty");
    else
    {
        tmp = head;
        head = tmp->next;
        tmp->next = NULL;
        delete tmp;
    }
}


void add_rear(int info)
{
    Node* tmp = new Node;
    tmp->info = info;
    tmp->next = NULL;

    Node* last = head;
    while (last->next != NULL) last = last->next;
    last->next = tmp;
}

void delete_rear()
{
    if (head->next == NULL)
    {
        delete head;
        head = NULL;
        return;
    }

    Node* last = head;
    while (last->next->next != NULL) last = last->next;
    delete last->next;
    last->next = NULL;
}


void add_after(Node *p, int info)
{
    Node* tmp = head;
    Node* newNode = new Node;
    newNode->info = info;

    while (tmp != p) {
        tmp = tmp->next;
    }
    newNode->next = tmp->next;
    tmp->next = newNode;
}

void delete_after(Node *p)
{
    if (head == p)
    {
        delete head;
        head = NULL;
    }
    if (p->next == NULL)
    {
        error("Letzter Node -> danach nichts zu loeschen!");
        return;
    }
    Node* post = p->next->next;
    if (post == NULL)
    {
        delete p->next;
        p->next = NULL;
        return;
    }

    delete p->next;
    p->next = post;

}

int anz_nodes()
{
    if (head == NULL) return 0;

    int retVal = 1;
    Node* tmp = head;
    while (tmp->next != NULL)
    {
        retVal++;
        tmp = tmp->next;
    }

    return(retVal);
}

void print()
{
    if (head == NULL)
    {
        error("no no node");
        return;
    }
    Node* tmp = head;
    cout << endl;
    do
    {
        cout << tmp->info << " ";
    } while ((tmp = tmp->next) != NULL);
    cout << endl;
}


int main()
{
  Node *p;

  add_front(3);
  add_front(2);
  add_front(1);
  add_rear(4);
  add_rear(5);
  cout << "Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten." << endl;
  print();

  delete_front();
  delete_rear();
  cout << "Die Liste sollte jetzt die Elemente 2, 3, 4 enthalten." << endl;
  print();

  delete_front();
  delete_rear();
  cout << "Die Liste sollte jetzt nur noch das Element 3 enthalten." << endl;
  print();

  delete_rear();
  cout << "Die Liste sollte jetzt leer sein." << endl;
  print();

  cout << "Jetzt muss eine Fehlermeldung kommen." << endl;
  delete_front();

  add_front(5);
  add_front(4);
  add_front(3);
  add_front(2);
  add_front(1);
  cout << "Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten." << endl;
  print();

  p = head->next->next;
  /* p sollte jetzt auf das Element mit dem Inhalt 3 zeigen. */
  add_after(p, 33);
  cout << "Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 4, 5 enthalten." << endl;
  print();

  p = p->next;
  /* p sollte jetzt auf das Element mit dem Inhalt 33 zeigen. */
  delete_after(p);
  cout << "Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 5 enthalten." << endl;
  print();

  p = p->next->next;
  /* p sollte jetzt den Wert NULL haben.
  (Knoten mit info 5 hat keinen Nachfolger.) */
  cout << "Jetzt sollte eine Fehlermeldung kommen." << endl;
  delete_after(p);

  return 0;

}




/*
Ausgabe des Programms:

Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten.
1	2	3	4	5
Die Liste sollte jetzt die Elemente 2, 3, 4 enthalten.
2	3	4
Die Liste sollte jetzt nur noch das Element 3 enthalten.
3
Die Liste sollte jetzt leer sein.

Jetzt muss eine Fehlermeldung kommen.
delete_front: list is empty
Die Liste sollte jetzt die Elemente 1, 2, 3, 4, 5 enthalten.
1	2	3	4	5
Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 4, 5 enthalten.
1	2	3	33	4	5
Die Liste sollte jetzt die Elemente 1, 2, 3, 33, 5 enthalten.
1	2	3	33	5
Jetzt sollte eine Fehlermeldung kommen.
delete_after: parameter p is NULL


*/
