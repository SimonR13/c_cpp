#include <iostream>

using namespace std;

const int MAX = 20;
struct queue {
    int anfang, ende, laenge;
    int daten[MAX];
} gQueue={0,0,0};

void enqueue(int elem);
int dequeue();
int anzEle();
int sumEle();



int main() {

    enqueue(12);
    enqueue(123);
    cout << "Anzahl: " << anzEle() << endl;
    cout << "Element: " <<  dequeue() << endl;
    cout << "Anzahl: " << anzEle() << endl;
    enqueue(124);
    cout << "Anzahl: " << anzEle() << endl;
    cout << "Element: " <<  dequeue() << endl;
    cout << "Element: " <<  dequeue() << endl;
    cout << "Element: " <<  dequeue() << endl;
    cout << "Element: " <<  dequeue() << endl;

    for (int i=1; i<MAX+3; i++) enqueue(i*MAX);
//    cout << "Anzahl: " << anzEle() << endl;
//    cout << "Summe: " << sumEle() << endl;
//
//    for (int i=0; i<(MAX/2); i++) cout << "Element: " << dequeue() << endl;
//
//    for (int i=1; i>(MAX); i++) enqueue(MAX/i);
//    cout << "Anzahl: " << anzEle() << endl;
//    cout << "Summe: " << sumEle() << endl;
//
    int tmp=dequeue();
     while (tmp >= 0) {
        cout << "Element: " << tmp << endl;
        tmp=dequeue();
    }

    return(0);
}

int anzEle() {
    return (gQueue.laenge);
}

int sumEle() {
    int summe=0;
    for (int i=0; i<anzEle(); i++) {
        summe += gQueue.daten[i];
    }
    return(summe);
}

void enqueue(int elem) {
    if (anzEle() < MAX) {
        int idx = gQueue.ende;
        gQueue.daten[idx]=elem;
        gQueue.ende = (++gQueue.ende == 20 ? 0 : (gQueue.ende) + 1 );
        gQueue.laenge++;
    }
    else {
        cout << "Element (" << elem << ") verworfen." << endl;
    }
}

int dequeue() {
    if (anzEle() >= 0) {
        int idx = gQueue.anfang;
        int tmp = gQueue.daten[idx];
        gQueue.anfang = (++gQueue.anfang == 20 ? 0 : (gQueue.anfang) +1);
        gQueue.laenge--;
        return tmp;
    }
    return -1;
}
