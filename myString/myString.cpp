#include <iostream>
#include <cstring>

using namespace std;

class myString {
    private:
        char *ptr;
        int len;
    public:
        myString() {ptr = NULL; len=0;}

        myString(char *str) {
            len = strlen(str)+1;
            ptr = (char *) new string[len+1];
            strcpy(ptr,str);
        }

        void ausgabe() { cout << ptr << endl; }

        ~myString() { if (ptr != NULL) delete ptr; }

        myString& operator= (const myString &s) {
            if (this == &s) { return(*this); }
            if (ptr != NULL) delete ptr;
            ptr = (char *) new string [len+1];
            strcpy(ptr, s.ptr);
            return(*this);
        }
};

int main() {
    myString s1("Hallo");
    myString s2;
    myString s3("Noch Einer");
    
    s1.ausgabe();
    s2.ausgabe();
    s3.ausgabe();

    s2 = s1;
    s1.ausgabe();
    s2.ausgabe();
    s3.ausgabe();

    s3 = s2;
    s1.ausgabe();
    s2.ausgabe();
    s3.ausgabe();

    return(0);
}
