#include <iostream>
#include <string>
#include <cmath>

using namespace std;

double atod(string s) {
    double retDouble;
    double vorzeichen = 1.0;

    while (s[0] == ' ') {
        s = s.substr(1, s.length());
    }

    if (s[0] == '-') {
        vorzeichen = -1.0;
        s = s.substr(1,s.length());
    }
    if (s[0] == '+') {
        vorzeichen = 1.0;
        s = s.substr(1,s.length());
    }
    if (s[0] == '.' || s[0] == ',') {
        retDouble = 0.0;
        for (double i=s.length(); i>1; i--) {
            if (s[i] == ' ') continue;
            retDouble += (pow(10.0, i) * static_cast<double>(s[i])) ;
        }
    }



    return(retDouble*vorzeichen);
}

int main() {
    string input;
    cout << "Double.parse: " << endl;
    cin >> input;
    cout << "Parsed: " << atod(input) << endl;
    return(0);
}
