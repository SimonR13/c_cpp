#include <iostream>
#include <string>

using namespace std;

void reverse(string &s) {
    char tmp;
    int ende = s.length();
    int anfang = 0;
    // gerade laenge
    if (ende-- % 2 == 0) {
        do {
            tmp = s[anfang];
            s[anfang++] = s[ende];
            s[ende--] = tmp;
        } while (ende > anfang);
    }
    // ungerade laenge
    else {
        while (anfang != ende) {
            tmp = s[anfang];
            s[anfang++] = s[ende];
            s[ende--] = tmp;
        }
    }
}

int main() {
    string word;
    cout << "Wort eingeben: ";
    getline(cin, word, '\n');
    reverse(word);
    cout << endl;
    cout << "Wort in reverse : " << word << endl;

    return(0);
}

