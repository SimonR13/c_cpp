#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace std;

enum Figur {
    DREIECK=1, RECHTECK=2, KREIS=3
};

struct Geo {
    Figur figur;
    double x,y,z;
};

void getGeo(Geo &retGeo) {
    char inFigur;

    cout << "Welche Figur? (D)reieck, (R)echteck oder (K)reis:";
    cin >> inFigur;
    cout << endl;

    switch (inFigur) {
        case 'D': case 'd':
            retGeo.figur = DREIECK;
            cout << "Seite a: ";
            cin >> retGeo.x;
            cout << endl << "Seite b: ";
            cin >> retGeo.y;
            cout << endl << "Eingeschlossener Winkel: ";
            cin >> retGeo.z;
            cout << endl;
            break;
        case 'R': case 'r':
            retGeo.figur = RECHTECK;
            cout << "Laenge: ";
            cin >> retGeo.x;
            cout << endl << "Breite: ";
            cin >> retGeo.y;
            cout << endl;
            break;
        case 'K': case 'k':
            retGeo.figur = KREIS;
            cout << "Radius: ";
            cin >> retGeo.x;
            cout << endl;
            break;
        default:
            cout << "Ende" << endl;
            exit(-1);
    }
}

void flaeche(Geo l_Geo) {
    // dreieck f = ((a*b*sin(winkel)) / 2)
    // rechteck f = a * b
    // kreis f = PI * r * r

    switch (l_Geo.figur) {
        case DREIECK:
            cout << "Dreiecksflaeche von (" << l_Geo.x << ", " << l_Geo.y << ", " << l_Geo.z << "): " << endl;
            cout << ((l_Geo.x * l_Geo.y * sin(l_Geo.z)) / double(2)) << endl;
            break;
        case RECHTECK:
            cout << "Rechtecksflaeche von (" << l_Geo.x << ", " << l_Geo.y << "): "  << endl;
            cout << (l_Geo.x * l_Geo.y) << endl;
            break;
        case KREIS:
            cout << "Kreisflaeche von (" << l_Geo.x << "): " << endl;
            cout << (M_PI * l_Geo.x * l_Geo.x) << endl;
            break;
        default:
            cerr << "Fehler, keine Figur erkannt!" << endl;
            exit(-1);
    }
}

int main()
{
    cout << "Figur erstellen..." << endl;
    Geo figur;
    getGeo(figur);
    flaeche(figur);
    return 0;
}
