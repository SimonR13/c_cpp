#include <iostream>
using namespace std;

#include "dynamic.h"

DynamicDemo f(DynamicDemo obj)
{
  DynamicDemo localObj(obj.getArraySize(), 111);

  return localObj;
}

int main()
{
  DynamicDemo obj1(10, 77), obj2;

  obj2 = f(obj1);
  cout << obj2.getArraySize() << endl;

  return 0;
}
