#include <cstdlib>

using namespace std;

#include "dynamic.h"

DynamicDemo::DynamicDemo(int size, int val):arraySize(size)
{
    arr = new int[arraySize];
    for (int i=0; i<arraySize; i++)
    {
        arr[i] = val;
    }
}

DynamicDemo::DynamicDemo(const DynamicDemo& obj)
{
    arraySize = obj.arraySize;
    arr = new int[arraySize];
    for (int i=0; i<arraySize; i++)
    {
        arr[i] = obj.arr[i];
    }
}

DynamicDemo::~DynamicDemo()
{
    arraySize = 0;
    delete[] arr;
}

DynamicDemo& DynamicDemo::operator= (const DynamicDemo& rhs)
{
//    if (this == &rhs) return(*this);
//    if (rhs.arraySize == 0) return *(new DynamicDemo());
    arraySize = rhs.arraySize;
    delete[] arr;
    arr = new int[arraySize];
    for (int i=0; i<arraySize; i++)
    {
        arr[i] = rhs.arr[i];
    }
    return(*this);
}
