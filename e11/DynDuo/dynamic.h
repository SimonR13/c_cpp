#ifndef DYNAMICDEMO_CLASS
#define DYNAMICDEMO_CLASS DYNAMICDEMO_CLASS

class DynamicDemo
{
   private:
      // dynamic array arr and number of elements
      int arraySize;
      int *arr;

   public:
      // constructor. array elements are initialized with value val
      DynamicDemo(int size = 0, int val = 0);

      // destructor
      ~DynamicDemo();

      // assignment operator
      DynamicDemo& operator=(const DynamicDemo& rhs);

      // copy constructor
      DynamicDemo(const DynamicDemo& obj);

      // access function
      inline int getArraySize() { return arraySize; }
};

#endif /* DYNAMICDEMO_CLASS */
