#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

class String
{
    private:
        char* ptr;
        unsigned len;
    public:
        String() {
            ptr = new char[1];
            len = 0;
            *ptr = 0x0;
        }
        String(const char* s) {
            if (s == NULL) s = "";
            len = strlen(s);
            ptr = new char[len+1];
            strcpy(ptr, s);
        }
        String(const String& s) {
            len = s.len;
            ptr = new char[len+1];
            strcpy(ptr, s.ptr);
        }
        ~String() {
            delete[] ptr;
        }

        String& operator= (const String& s) {
            if (this == &s) return(*this);
            delete[] ptr;
            len = s.len;
            ptr = new char[len+1];
            strcpy(ptr, s.ptr);
            return(*this);
        }
        String& operator= (const char* s) {
            if (ptr == s) return (*this);
            if (s == NULL) s = "";
            delete[] ptr;
            len = strlen(s);
            ptr = new char[len+1];
            strcpy(ptr, s);
            return(*this);
        }

        friend ostream& operator<< (ostream& ostr, const String& s) {
            return(ostr << s.ptr << endl);
        }

        char* getPtr() const { return ptr; }
};

int main()
{
    String a;
    String b = a;
    String c = "Kernighan";
    String d = String("Ritchie");
    String e = (char*) 0;

    a = "Waalkes";
    a = b = c;
    d = d;
    d = d.getPtr();

    cout << c << d;

    return 0;
}
