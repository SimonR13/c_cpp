#include <iostream>
#include <iomanip>
using namespace std;

const int JahresZahl = 2012;
const int Jahre = 20;
const float Zins = 5;
const float Einzahlung = 5000;

int main() {
	float Kapital = Einzahlung;
	for (int i=0; i<Jahre; i++) {
		cout << setw(4) << i+JahresZahl << " - "
				<< setw(12) << Kapital << endl;
		Kapital += Kapital * Zins / 100 + Einzahlung;
	}
}

