#include <iostream>
#include <stdlib.h>
using namespace std;

int main() {
	int RndNo=0, GuessNo=0;
	bool RightGuess=false;

	while (RightGuess == false) {
		cout << "Bitte raten Sie eine Zahl zwischen 1 und 1000: " << endl;
		cin >> GuessNo;

			srand(GuessNo);
			RndNo = rand() % 1000 + 1;
			cout << "RndNo: " << RndNo << endl;

		if (GuessNo == RndNo) {
			cout << "Richtig! Die Zahl war: " << RndNo;
			return 0;
		} else if (GuessNo < RndNo) {
			cout << "Die Zahl war um " << RndNo - GuessNo << " größer." << endl;
		} else if (GuessNo > RndNo) {
			cout << "Die Zahl war um " << GuessNo - RndNo << " kleiner." << endl;
		}
	}
}
